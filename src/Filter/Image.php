<?php
namespace Filter;

use PHPThumb\GD;
use Zend\Filter\AbstractFilter;

/**
 * Class Image
 * @package Filter
 */
class Image extends AbstractFilter {

    /**
     * @var string
     */
    protected $mode;
    /**
     * @var int
     */
    protected $width;
    /**
     * @var int
     */
    protected $height;

    /**
     * @var string
     */
    protected $target;

    /**
     * @var array
     */
    protected $adapterOptions = array();

    /**
     * @param $fileName
     * @param array $options
     * @param array $plugins
     * @return GD
     */
    public function create($fileName, $options = array(), array $plugins = array())
    {
        return new GD($fileName, $options, $plugins);
    }

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * @param mixed $filePath
     * @return GD
     */
    public function filter($filePath)
    {
        if( is_array($filePath) && isset($filePath['tmp_name']))
        {
            $filePath = array_pop(explode('/', $filePath['tmp_name']));
        }

        $resource = $this->create($this->getTarget() . $filePath, $this->getAdapterOptions());

        switch( $this->getMode() )
        {
            case 'crop':
            {
                $resource->adaptiveResize($this->getWidth(), $this->getHeight());
                break;
            }

            default:
                {
                $resource->resize($this->getWidth(), $this->getHeight());
                }
        }

        $resource->save($this->getTarget() . $filePath);

        return $this->getTarget() . $filePath;
    }

    /**
     * @return string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param string $target
     */
    public function setTarget($target)
    {
        $target = str_ireplace(array('/', '\\'), DIRECTORY_SEPARATOR, $target);

        if( substr($target, -1) != DIRECTORY_SEPARATOR )
        {
            $target .= DIRECTORY_SEPARATOR;
        }

        $this->target = $target;
    }

    /**
     * @return array
     */
    public function getAdapterOptions()
    {
        return $this->adapterOptions;
    }

    /**
     * @param array $adapterOptions
     */
    public function setAdapterOptions($adapterOptions)
    {
        $this->adapterOptions = $adapterOptions;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param mixed $mode
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }
}