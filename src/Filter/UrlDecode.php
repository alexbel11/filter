<?php
/**
 * Created by zdi design group
 * http://www.zdidesigngroup.com
 *
 * User: derekmiranda
 * Date: 7/23/14
 * Time: 4:20 PM
 * Project: filter
 */
namespace Filter;
use Zend\Filter\AbstractFilter;
use Zend\Filter\Word\SeparatorToSeparator;

/**
 * Class UrlEncode
 * @package Filter
 */
class UrlDecode extends AbstractFilter {

    /**
     * @param $value
     * @return string
     */
    public function filter($value)
    {
        $filter = new SeparatorToSeparator('-', ' ');
        return $filter->filter(urldecode($value));
    }
} 