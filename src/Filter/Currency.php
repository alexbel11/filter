<?php

namespace Filter;

use Zend\Filter\AbstractFilter;
use Zend\I18n\View\Helper\CurrencyFormat;

/**
 * Class Currency
 * @package Filter
 */
class Currency extends AbstractFilter {

    /**
     * @var string
     */
    protected $currencyCode;
    /**
     * @var string
     */
    protected $locale;
    /**
     * @var bool
     */
    protected $showDecimal;

    /**
     * Constructor
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * Formats the currency
     * @param float|int|string $value
     * @return mixed
     */
    public function filter($value)
    {
        $currency = new CurrencyFormat();
        $currency->setCurrencyCode($this->currencyCode)->setLocale($this->locale);
        $currency->setShouldShowDecimals($this->showDecimal);

        return $currency($value);
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @param string $currencyCode
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return boolean
     */
    public function isShowDecimal()
    {
        return $this->showDecimal;
    }

    /**
     * @param boolean $showDecimal
     */
    public function setShowDecimal($showDecimal)
    {
        $this->showDecimal = $showDecimal;
    }
}