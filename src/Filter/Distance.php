<?php
/**
 * Created by zdi design group
 * http://www.zdidesigngroup.com
 *
 * User: derekmiranda
 * Date: 9/16/14
 * Time: 3:03 PM
 * Project: filter
 */
namespace Filter;

use Zend\Filter\AbstractFilter;

/**
 * Class Distance
 * @package Filter
 */
class Distance extends AbstractFilter {

    /**
     * @var int
     */
    protected $decimals = 1;

    /**
     * @var string
     */
    protected $descriptor = 'Mi';

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * @param mixed $value
     * @return string
     */
    public function filter($value)
    {
        return number_format($value, $this->getDecimals()) . ' ' . $this->getDescriptor();
    }

    /**
     * @return mixed
     */
    public function getDecimals()
    {
        return $this->decimals;
    }

    /**
     * @param mixed $decimals
     */
    public function setDecimals($decimals)
    {
        $this->decimals = $decimals;
    }

    /**
     * @return string
     */
    public function getDescriptor()
    {
        return $this->descriptor;
    }

    /**
     * @param string $descriptor
     */
    public function setDescriptor($descriptor)
    {
        $this->descriptor = $descriptor;
    }
} 