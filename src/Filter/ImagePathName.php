<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 11/29/13
 * Time: 9:36 PM
 */

namespace Filter;
use Zend\Filter\AbstractFilter;

/**
 * Class ImagePathName
 * @package Filter
 */
class ImagePathName extends AbstractFilter {

    /**
     * @var bool
     */
    protected $keepPublicDirectory = false;

    /**
     * Constructor
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * Filters the path name to resolve correctly in the public directory
     * @param mixed $value
     * @return mixed
     */
    public function filter($value)
    {
        if( stristr($value, '../'))
        {
            // we have something relative, let's lose the relative part
            $value = array_pop(explode('../', $value));
        }

        if(!$this->getKeepPublicDirectory())
        {
            return str_ireplace(array('./public/', '/public/', 'public/'), '/', $value);
        }

        return str_ireplace(array('./public/', '/public/'), 'public/', $value);
    }

    /**
     * @return boolean
     */
    public function getKeepPublicDirectory()
    {
        return $this->keepPublicDirectory;
    }

    /**
     * @param boolean $keepPublicDirectory
     */
    public function setKeepPublicDirectory($keepPublicDirectory)
    {
        $this->keepPublicDirectory = $keepPublicDirectory;
    }
} 