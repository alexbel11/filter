<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 11/30/13
 * Time: 1:47 PM
 */
namespace Filter;

use Zend\Filter\AbstractFilter;

/**
 * Class FileSize
 * @package Filter
 */
class FileSize extends AbstractFilter {

    /**
     * @var int
     */
    protected $precision;

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * @param $bytes
     * @return string
     */
    public function filter($bytes)
    {
        $precision = $this->precision;

        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);
        $bytes /= pow(1024, $pow);

        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    /**
     * @return int
     */
    public function getPrecision()
    {
        return $this->precision;
    }

    /**
     * @param int $precision
     */
    public function setPrecision($precision)
    {
        $this->precision = $precision;
    }
}