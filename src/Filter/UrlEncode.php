<?php
/**
 * Created by zdi design group
 * http://www.zdidesigngroup.com
 *
 * User: derekmiranda
 * Date: 7/23/14
 * Time: 4:20 PM
 * Project: filter
 */
namespace Filter;
use Zend\Filter\AbstractFilter;
use Zend\Filter\Word\SeparatorToDash;

/**
 * Class UrlEncode
 * @package Filter
 */
class UrlEncode extends AbstractFilter {

    /**
     * @param $value
     * @return string
     */
    public function filter($value)
    {
        $filter = new SeparatorToDash(' ');
        $filter2 = new SeparatorToDash('/');

        return urlencode($filter->filter($filter2->filter($value)));
    }
} 