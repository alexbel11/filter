<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 2/27/14
 * Time: 3:04 PM
 */
return array(
    'filters'=>array(
        'invokables'=>array(
            'Filter\Currency'                    =>'Filter\Currency',
            'Filter\DatabaseTableName'           =>'Filter\DatabaseTableName',
            'Filter\FileSize'                    =>'Filter\FileSize',
            'Filter\Float'                       =>'Filter\Float',
            'Filter\IdFilter'                    =>'Filter\IdFilter',
            'Filter\Image'                       =>'Filter\Image',
            'Filter\ImagePathName'               =>'Filter\ImagePathName',
            'Filter\IntToYesNo'                  =>'Filter\IntToYesNo',
            'Filter\JsonDecode'                  =>'Filter\JsonDecode',
            'Filter\JsonEncode'                  =>'Filter\JsonEncode',
            'Filter\ModuleControllerActionObject'=>'Filter\ModuleControllerActionObject',
            'Filter\NumberFormat'                =>'Filter\NumberFormat',
            'Filter\Phone'                       =>'Filter\Phone',
            'Filter\QrCodeFilter'                =>'Filter\QrCodeFilter',
            'Filter\QueryParamToArray'           =>'Filter\QueryParamToArray',
            'Filter\StringToImage'               =>'Filter\StringToImage',
            'Filter\StringTruncate'              =>'Filter\StringTruncate',
            'Filter\TinyCrypt\Encrypt'           =>'Filter\TinyCrypt\Encrypt',
            'Filter\TinyCrypt\Decrypt'           =>'Filter\TinyCrypt\Decrypt',
            'Filter\UpperCaseWords'              =>'Filter\UpperCaseWords',
            'Filter\UrlEncode'                   =>'Filter\UrlEncode',
            'Filter\UrlDecode'                   =>'Filter\UrlDecode',
        ),
    ),

    /**
     * Filter MetaData
     */
    'filter_metadata' => array(
        'Filter\Currency'=> array(
            'show_decimal' => 'bool',
            'locale' => 'string',
            'currency_code'=>'string'
        ),
        'Filter\DatabaseTableName'=> array(),
        'Filter\FileSize' => array(
            'precision'=>'int'
        ),
        'Filter\Float'=> array(
            'decimals'=>'int',
        ),
        'Filter\Image'=> array(
            'mode'=>'string',
            'width'=>'int',
            'height'=>'int',
            'target'=>'string'
        ),
        'Filter\IdFilter'=>array(
            'crypt'=>'bool',
            'crypt_method'=>'string'
        ),
        'Filter\ImagePathName'=> array(
            'keep_public_directory'=>'bool'
        ),
        'Filter\IntToYesNo'                  => array(),
        'Filter\JsonDecode'                  => array(),
        'Filter\JsonEncode'                  => array(),
        'Filter\ModuleControllerActionObject'=> array(
            'str_to_lower'=>'bool',
        ),
        'Filter\NumberFormat'                => array(
            'decimals'=>'int',
            'decimal_point'=>'string',
            'thousands_separator'=>'string',
        ),
        'Filter\Phone'=>array(),
        'Filter\QrCodeFilter'=> array(
            'destination'=>'string',
            'mode'=>'string',
            'file_name'=>'string',
            'size'=>'int',
            'use_value_as_file_name'=>'bool',
            'filter_return_path'=>'bool',
        ),
        'Filter\QueryParamToArray'=> array(
            'separator' => 'string'
        ),
        'Filter\StringToImage'               => array(),
        'Filter\StringTruncate'                => array(
            'string_length'=>'int',
            'use_word_wrap'=>true,
            'append_ellipsis'=>true,
        ),
        'Filter\TinyCrypt\Encrypt'=> array(
            'length'=>'int',
            'key'=>'string'
        ),
        'Filter\TinyCrypt\Decrypt'=> array(
            'length'=>'int',
            'key'=>'string'
        ),
        'Filter\UpperCaseWords' => array(
            'string_to_lower'=>'bool'
        ),
        'Filter\UrlEncode' => array(),
        'Filter\UrlDecode' => array(),
    ),
);